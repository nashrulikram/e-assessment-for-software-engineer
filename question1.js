document.addEventListener("DOMContentLoaded", function() {
    // ADD FUNCTION
    function submitForm(event) {
        event.preventDefault(); // GET DEFAULT VALUE

        // GET INPUT
        var fullname = document.getElementById("fullname").value;
        var dob = document.getElementById("dob").value;
        var address = document.getElementById("address").value;
        var postcode = document.getElementById("postcode").value;

        // TO SEND THE DATA
        var data = {
            fullname: fullname,
            dob: dob,
            address: address,
            postcode: postcode
        };

        // AJAX SUBMIT DATA TO https://xxxxxxx/user/registration
        var xhr = new XMLHttpRequest();
        var url = "https://xxxxxxx/user/registration";
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");

        // SENDING THE DATA
        xhr.send(JSON.stringify(data));
        
        // SUCCESFULLY 
        console.log("Registration successful");
    }

    // AFTER CLICK THE SUBMIT
    document.getElementById("input-form").addEventListener("submit", submitForm);
});
