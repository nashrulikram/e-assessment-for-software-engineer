SELECT 
    u.fullname AS Fullname, 
    --SELECT FULL NAME 
    TIMESTAMPDIFF(YEAR, u.dob, CURDATE()) AS Age,
    --CALCULATE THE AGE
    u.address AS Address,
    --SELECT ADDRESS
    s.postcode AS Postcode,
    --SELECT POSTCODE
    s.name AS State_Name
    --SELECT STATE
FROM 
    User u
JOIN 
    State s ON u.state_id = s.id;
    --JOIN USER TABLE AND STATE TABLE
