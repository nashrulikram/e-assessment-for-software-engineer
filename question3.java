public class Main {
    public static void main(String[] args) {
        //DEFINE ARRAY GIVEN
        int[] numbers = {100, 200, 300, 400, 800, 900, 1000, 3, 5, 9};

        //CALL THE METHOD TO DO OPERATION ADD
        int sum = findSum(numbers);

        //SHOW THE OUPUT
        System.out.println("Sum of numbers: " + sum);
    }


    //METHOD TO FIND OPERATION ADD
    public static int findSum(int[] arr) {

        //INITIALIZE SUM VARIABLE
        int sum = 0;

        //PROCESS ADD ALL ARRAY NUMBER
        for (int num : arr) {
            sum += num;
        }
        return sum;
    }
}
